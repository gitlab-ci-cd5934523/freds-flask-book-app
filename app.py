"""
A simple Flask application for managing a book collection.
"""

from flask import Flask, render_template, request, redirect, jsonify

app = Flask(__name__)

books = [
    {
        "id": 1,
        "title": "To Kill a Mockingbird",
        "author": "Harper Lee",
        "pages": 281,
        "genre": "Fiction",
        "summary": (
            "Set in the 1930s Deep South, a young girl named Scout Finch lives with her brother," 
            "Jem, and their father,"
            "Atticus. Atticus is a lawyer defending a black man accused of raping a white woman." 
            "The story explores themes"
            "of racial injustice and moral growth."
        )


    },
    # Other books...
]

@app.route('/')
def index():
    """Renders the index page with the list of books."""
    return render_template('index.html', books=books)

@app.route('/books')
def get_books():
    """Returns a JSON list of all books."""
    return jsonify(books)

@app.route('/book/<int:book_id>')
def get_book(book_id):
    """Returns a JSON object of a specific book by ID."""
    book = next((book for book in books if book['id'] == book_id), None)
    if book:
        return jsonify(book)
    return jsonify({'error': 'Book not found'}), 404

@app.route('/add_book', methods=['GET', 'POST'])
def add_book():
    """Provides a form to add a new book and handles the form submission."""
    if request.method == 'POST':
        new_book = {
            'id': len(books) + 1,
            'title': request.form['title'],
            'author': request.form['author'],
            'pages': request.form['pages'],
            'genre': request.form['genre'],
            'summary': request.form['summary']
        }
        books.append(new_book)
        return redirect('/')
    return render_template('add_book.html')

@app.route('/update_book/<int:book_id>', methods=['GET', 'POST'])
def update_book(book_id):
    """Provides a form to update a book by ID and handles the form submission."""
    book = next((book for book in books if book['id'] == book_id), None)
    if not book:
        return jsonify({'error': 'Book not found'}), 404

    if request.method == 'POST':
        book['title'] = request.form['title']
        book['author'] = request.form['author']
        book['pages'] = request.form['pages']
        book['genre'] = request.form['genre']
        book['summary'] = request.form['summary']
        return redirect('/')
    return render_template('update_book.html', book=book)

@app.route('/delete_book/<int:book_id>', methods=['POST'])
def delete_book(book_id):
    """Handles the deletion of a book by ID."""
    global books # pylint: disable=global-statement
    books = [book for book in books if book['id'] != book_id]
    return redirect('/')

if __name__ == '__main__':
    app.run(debug=True)
