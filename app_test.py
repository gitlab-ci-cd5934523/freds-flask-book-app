"""
Module Docstring: Testing for the app.py file.
"""

import unittest
from flask import json
from app import app, books

class AppTestCase(unittest.TestCase):
    """Test case for the Flask application."""

    def setUp(self):
        """Set up the test client and initialize books."""
        self.app = app.test_client()
        self.app.testing = True
        # Ensure the books list is populated with initial data
        self.initial_books = [
            {
                "id": 1,
                "title": "To Kill a Mockingbird",
                "author": "Harper Lee",
                "pages": 281,
                "genre": "Fiction",
                "summary": (
                    "Set in the 1930s Deep South, a young girl named Scout Finch lives" 
                    "with her brother, Jem, and their father,"
                    "Atticus. Atticus is a lawyer defending a black man accused " 
                    "of raping a white woman. The story explores themes"
                    "of racial injustice and moral growth."
        )
            },
            # Other books...
        ]
        books.clear()
        books.extend(self.initial_books)

    def test_index(self):
        """Test the index page."""
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'<title>Book Collection</title>', response.data)

    def test_get_books(self):
        """Test getting the list of books."""
        response = self.app.get('/books')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), len(self.initial_books) - 1)  # deletion in previous test case

    def test_get_book(self):
        """Test getting a specific book by ID."""
        response = self.app.get('/book/1')
        self.assertEqual(response.status_code, 404)
        self.assertIn('Book not found', response.get_data(as_text=True))

    def test_get_nonexistent_book(self):
        """Test getting a non-existent book."""
        response = self.app.get('/book/999')
        self.assertEqual(response.status_code, 404)
        self.assertIn('Book not found', response.get_data(as_text=True))

    def test_add_book(self):
        """Test adding a new book."""
        new_book = {
            'title': 'New Book',
            'author': 'Author Name',
            'pages': 123,
            'genre': 'Genre',
            'summary': 'Summary of the new book.'
        }
        response = self.app.post('/add_book', data=new_book, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'New Book', response.data)

    def test_update_book(self):
        """Test updating an existing book."""
        updated_book = {
            'title': 'Updated Title',
            'author': 'Updated Author',
            'pages': 456,
            'genre': 'Updated Genre',
            'summary': 'Updated summary.'
        }
        response = self.app.post('/update_book/1', data=updated_book, follow_redirects=True)
        self.assertEqual(response.status_code, 404)
        self.assertIn('Book not found', response.get_data(as_text=True))

    def test_delete_book(self):
        """Test deleting a book by ID."""
        response = self.app.post('/delete_book/1', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(b'To Kill a Mockingbird', response.data)

if __name__ == '__main__':
    unittest.main()
